<?php

namespace Tests\Feature;

use App\Models\DishType;
use App\Models\Ingredient;
use App\Models\Recipe;
use App\Models\Role;
use App\Models\User;
use Illuminate\Foundation\Testing\RefreshDatabase;
use Tests\TestCase;

class HomePageTest extends TestCase
{
    use RefreshDatabase;

    public function test_home_page_contains_empty_table(): void
    {
        $response = $this->get('/');

        $response->assertStatus(200);
        $response->assertSee('You can create first recipe');
    }

    public function test_home_page_contains_non_empty_table(): void
    {
        $this->prepareDatabase();

        $recipe = Recipe::factory()->create([
            'name' => 'Pasta carbonara',
            'description' => 'recipe',
        ]);

        $response = $this->get('/');

        $response->assertStatus(200);
        $response->assertDontSee('You can create first recipe');
        $response->assertViewHas('recipes', function ($collection) use ($recipe) {
            return $collection->contains($recipe);
        });
    }

    public function test_paginated_recipes_table_doesnt_contain_6th_record()
    {
        $this->prepareDatabase();

        $recipes = Recipe::factory(6)->create();
        $lastRecipe = $recipes->last();

        $response = $this->get('/');

        $response->assertStatus(200);
        $response->assertViewHas('recipes', function ($collection) use ($lastRecipe) {
            return ! $collection->contains($lastRecipe);
        });
    }

    public function test_specific_recipe_page()
    {
        $this->prepareDatabase();

        $recipe = Recipe::factory()->create([
            'name' => 'Pasta carbonara',
            'description' => 'recipe',
        ]);

        $response = $this->get('/show/'.$recipe->id);
        $response->assertStatus(200);
        $response->assertSeeText($recipe->name);
        $response->assertSeeText($recipe->description);
    }

    private function prepareDatabase(): void
    {
        User::factory()->create([
            'name' => 'user',
            'email' => 'user@user.com',
            'role_id' => Role::IS_USER,
        ]);
        DishType::factory()->create();
        Ingredient::factory()->create();
    }
}
