<?php

namespace Tests\Feature;

use App\Models\Role;
use App\Models\User;
use Database\Seeders\RoleSeeder;
use Illuminate\Foundation\Testing\RefreshDatabase;
use Tests\TestCase;

class RecipesTest extends TestCase
{
    use RefreshDatabase;

    private User $user;

    public function setUp(): void
    {
        parent::setUp();

        $this->user = $this->createUser();
    }

    public function test_unauthenticated_user_cannot_see_my_recipes_and_write_new_recipe_buttons()
    {
        $response = $this->get('/');

        $response->assertDontSee('My Recipes');
        $response->assertDontSee('+ Write new recipe');
    }

    public function test_unauthenticated_user_cannot_access_my_recipes_and_write_new_recipe_pages()
    {
        $response = $this->get('/user/recipe');
        $response->assertStatus(302);
        $response->assertRedirect('login');

        $response = $this->get('/user/recipe/create');
        $response->assertStatus(302);
        $response->assertRedirect('login');
    }

    public function test_authenticated_user_can_see_my_recipes_and_write_new_recipe_buttons()
    {
        $response = $this->actingAs($this->user)->get('/');

        $response->assertSee('My Recipes');
        $response->assertSee('+ Write new recipe');

    }

    public function test_authenticated_user_can_access_my_recipes_and_write_new_recipe_pages()
    {
        $response = $this->actingAs($this->user)->get('/user/recipe');
        $response->assertStatus(200);

        $response = $this->actingAs($this->user)->get('/user/recipe/create');
        $response->assertStatus(200);
    }

    private function createUser(): User
    {
        $this->seed(RoleSeeder::class);

        return User::factory()->create([
            'name' => 'user',
            'email' => 'user@user.com',
            'role_id' => Role::IS_USER,
        ]);
    }
}
