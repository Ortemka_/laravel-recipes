<?php

namespace Database\Seeders;

use App\Models\Role;
use App\Models\User;
use Illuminate\Database\Seeder;

class UserSeeder extends Seeder
{
    /**
     * Run the database seeds.
     */
    public function run(): void
    {
        $admin = User::factory()->create([
            'name' => 'admin',
            'email' => 'admin@admin.com',
            'role_id' => Role::IS_ADMIN,
        ]);

        $user = User::factory()->create([
            'name' => 'user',
            'email' => 'user@user.com',
            'role_id' => Role::IS_USER,
        ]);

        $user = User::factory()->create([
            'name' => 'user1',
            'email' => 'user1@user1.com',
            'role_id' => Role::IS_USER,
        ]);
    }
}
