<?php

namespace Database\Seeders;

use App\Models\DishType;
use Illuminate\Database\Seeder;

class DishTypeSeeder extends Seeder
{
    /**
     * Run the database seeds.
     */
    public function run(): void
    {
        //        DishType::factory()->count(4)->create();
        DishType::create(['name' => 'Main Dish']);
        DishType::create(['name' => 'Desserts']);
        DishType::create(['name' => 'Appetizers']);
    }
}
