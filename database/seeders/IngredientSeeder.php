<?php

namespace Database\Seeders;

use App\Models\Ingredient;
use Illuminate\Database\Seeder;

class IngredientSeeder extends Seeder
{
    /**
     * Run the database seeds.
     */
    public function run(): void
    {
        Ingredient::create(['name' => 'Potato']);
        Ingredient::create(['name' => 'Garlic clove']);
        Ingredient::create(['name' => 'Fresh basil']);
        Ingredient::create(['name' => 'Parmesan']);
        Ingredient::create(['name' => 'Cherry tomatoes']);
        Ingredient::create(['name' => 'Basil leaves']);
        Ingredient::create(['name' => 'Bread flour']);
    }
}
