<?php

namespace Database\Factories;

use App\Models\DishType;
use App\Models\User;
use Illuminate\Database\Eloquent\Factories\Factory;

/**
 * @extends \Illuminate\Database\Eloquent\Factories\Factory<\App\Models\Recipe>
 */
class RecipeFactory extends Factory
{
    /**
     * Define the model's default state.
     *
     * @return array<string, mixed>
     */
    public function definition(): array
    {
        $wordsNumber = rand(1, 3);

        return [
            'name' => $this->faker->words($wordsNumber, true),
            'description' => $this->faker->text(),
            'image' => 'example.jpg',
            'dish_type_id' => DishType::all()->random()->id,
            'author_id' => User::where('name', 'LIKE', '%'.'user'.'%')->get()->random()->id,
        ];
    }
}
