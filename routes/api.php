<?php

use App\Http\Controllers\Api\V1\DishTypeController;
use App\Http\Controllers\Api\V1\IngredientController;
use App\Http\Controllers\Api\V1\RecipeController;
use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider and all of them will
| be assigned to the "api" middleware group. Make something great!
|
*/

Route::prefix('v1')->group(function () {
    Route::apiResource('dish-type', DishTypeController::class)
        ->only(['index', 'show']);

    Route::apiResource('recipe', RecipeController::class)
        ->only(['store', 'update', 'show']);

    Route::apiResource('ingredient', IngredientController::class)
        ->only(['index', 'store', 'update', 'destroy']);
});
