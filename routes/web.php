<?php

use App\Http\Controllers\Admin\IngredientController as AdminIngredientController;
use App\Http\Controllers\User\RecipeController as UserRecipeController;
use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider and all of them will
| be assigned to the "web" middleware group. Make something great!
|
*/

Route::get('/', [App\Http\Controllers\HomeController::class, 'index'])->name('home');
Route::get('/show/{recipe}', [App\Http\Controllers\HomeController::class, 'show'])->name('show');

Route::prefix('/user')->middleware(['auth', 'role:user'])->name('user.')->group(function () {
    Route::resource('recipe', UserRecipeController::class);
});

Route::prefix('/admin')->middleware(['auth', 'role:admin'])->name('admin.')->group(function () {
    Route::resource('ingredient', AdminIngredientController::class)->only('index');
});

Auth::routes();
