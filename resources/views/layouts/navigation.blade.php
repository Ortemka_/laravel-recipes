
<ul class="navbar-nav me-auto">

    @can('user', \App\Models\Recipe::class)
        <li class="nav-item"><a href="{{ route('user.recipe.index') }}" class="nav-link ms-2 {{ request()->routeIs('user.recipe.index') ? 'active' : '' }}">My Recipes</a></li>
        <li class="nav-item"><a href="{{ route('user.recipe.create') }}" class="nav-link ms-2 {{ request()->routeIs('user.recipe.create') ? 'active' : '' }}">+ Write new recipe</a></li>
    @endcan

    @can('admin', \App\Models\Ingredient::class)
        <li class="nav-item"><a href="{{ route('admin.ingredient.index') }}" class="nav-link ms-2 {{ request()->routeIs('admin.ingredient.index') ? 'active' : '' }}">Ingredients</a></li>
    @endcan

</ul>
