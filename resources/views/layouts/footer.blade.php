
<div class="container">
    <footer class="py-3 my-4">
        <ul class="nav justify-content-center border-bottom pb-3 mb-3">
            <li class="nav-item"><a href="{{ route('home') }}" class="nav-link px-2 text-body-secondary">Home</a></li>
            @can('user', \App\Models\Recipe::class)
                <li class="nav-item"><a href="{{ route('user.recipe.index') }}" class="nav-link ms-2 {{ request()->routeIs('user.recipe.index') ? 'active' : '' }}">My Recipes</a></li>
                <li class="nav-item"><a href="{{ route('user.recipe.create') }}" class="nav-link ms-2 {{ request()->routeIs('user.recipe.create') ? 'active' : '' }}">+ Write new recipe</a></li>
            @endcan

            @can('admin', \App\Models\Ingredient::class)
                <li class="nav-item"><a href="{{ route('admin.ingredient.index') }}" class="nav-link ms-2 {{ request()->routeIs('admin.ingredient.index') ? 'active' : '' }}">Ingredient</a></li>
            @endcan
        </ul>
        <p class="text-center text-body-secondary">© 2023 Recipes</p>
    </footer>
</div>
