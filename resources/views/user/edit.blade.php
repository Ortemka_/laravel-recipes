@extends('layouts.app')

@section('content')
    <edit-recipe-component user-id="{{ $userId }}" recipe-id="{{ $recipeId }}"></edit-recipe-component>
@endsection

