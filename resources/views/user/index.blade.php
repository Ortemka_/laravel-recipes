@extends('layouts.app')

@section('content')
    <div class="row justify-content-center">
        <div class="col-md-8">
            <div class="card">
                <div class="card-header h4 text-center">All my recipes</div>
                <div class="card-body">
                    <div class="container pt-2">

                        @if(!$recipes->isEmpty())

                            @foreach($recipes as $recipe)
                                <div class="col-md-12 border-bottom border-secondary border-2 mb-1 rounded-4">
                                    <div class="row ms-2 mt-1">
                                        <div class="col-md-3 pt-5">
                                            <img style="max-width: 100%; height: auto;"
                                                 src="{{ asset(('/storage/images/recipes/' . $recipe->image)) }}"
                                                 class="img-thumbnail" alt="{{ $recipe->name }}">
                                        </div>

                                        <div class="col-md-9">
                                            <h2>
                                                <a class="link-dark text-decoration-none"
                                                   href="{{ route('user.recipe.show', $recipe) }}">{{ $recipe->name }}</a>
                                            </h2>
                                            <ul class="list-unstyled text-small">
                                                <li>
                                                    {{--                                                        Author: <strong>{{ $recipe->author->name }}</strong>,--}}
                                                    Type: <strong>{{ $recipe->dishType->name }}</strong>
                                                </li>
                                            </ul>
                                            <p class="fs-5">{{ Str::limit($recipe->description, 200) }}</p>

                                            <div class="d-grid gap-2 d-md-flex justify-content-md-end mb-2 me-2">
                                                <form method="post" class=""
                                                      action="{{ route('user.recipe.destroy', $recipe) }}">
                                                    @csrf
                                                    @method('DELETE')
                                                    <a href="{{ route('user.recipe.edit', $recipe->id) }}"
                                                       class="btn btn-outline-primary">Edit</a>
                                                    <button type="submit"
                                                            class="btn btn-outline-danger ms-2 text-end"
                                                            onclick=" return confirm('Are you sure you wish to delete this recipe ?')">
                                                        Delete
                                                    </button>
                                                </form>
                                            </div>

                                        </div>
                                    </div>
                                </div>
                            @endforeach
                            <div class="justify-content-center">{{ $recipes->links() }}</div>
                    </div>
                    @else
                        <p class="mt-4">Make your first recipe</p>
                    @endif
                </div>
            </div>
        </div>
    </div>
@endsection
