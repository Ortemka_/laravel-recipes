@extends('layouts.app')

@section('content')
    <div class="row justify-content-center">
        <div class="col-md-12">
            <div class="card">
                <div class="h5 card-header text-center">All recipes</div>

                <div class="card-body">
                    <div class="container pt-2">
                        @if(!$recipes->isEmpty())

                            @foreach($recipes as $recipe)

                                <div class="col-md-12 border-bottom border-secondary border-2 mb-3 rounded-4">
                                    <div class="row ms-2 mt-2">
                                        <div class="col-md-3 mb-2">
                                            <img style="max-width: 100%; height: auto;"
                                                 src="{{ asset(('/storage/images/recipes/' . $recipe->image)) }}"
                                                 class="img-thumbnail" alt="{{ $recipe->name }}">
                                        </div>

                                        <div class="col-md-9">
                                            <h2>
                                                <a class="link-dark text-decoration-none"
                                                   href="{{ route('show', $recipe) }}">{{ $recipe->name }}</a>
                                            </h2>
                                            <ul class="list-unstyled text-small">
                                                <li>
                                                    Author: <strong>{{ $recipe->author->name }}</strong>,
                                                    <br>
                                                    Category: <strong>{{ $recipe->dishType->name }}</strong>
                                                </li>
                                            </ul>
                                            <p class="fs-5">{{ Str::limit($recipe->description, 200) }}</p>

                                        </div>
                                    </div>
                                </div>

                            @endforeach
                            <div class="justify-content-center">{{ $recipes->links() }}</div>

                        @else
                            <p class="mt-2 h3 text-center">You can create first recipe</p>
                        @endif
                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection
