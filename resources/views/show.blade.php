@extends('layouts.app')

@section('content')
    <div class="row justify-content-center">
        <div class="col-md-8">
            <div class="card">
                <div class="card-header h4 text-center">Recipe of <b>{{ $recipe->name }}</b></div>

                <div class="card-body">
                    <div class="container pt-2">

                        <div class="col-md-12 border-start border-secondary border-2 mb-3 rounded-4">
                            <div class="row ms-2 mt-2 pt-4">
                                <div class="col-md-3">
                                    <img style="max-width: 100%; height: auto;"
                                         src="{{ asset(('/storage/images/recipes/' . $recipe->image)) }}"
                                         class="img-thumbnail" alt="{{ $recipe->name }}">
                                </div>

                                <div class="col-md-9">

                                    <ul class="list-unstyled text-small">
                                        <li>
                                            Author: <strong>{{ $recipe->author->name }}</strong>,
                                            <br>
                                            Type: <strong>{{ $recipe->dishType->name }}</strong>
                                        </li>
                                    </ul>
                                    <p class="fs-5"><strong>Instruction:</strong> {{ $recipe->description }}</p>

                                    <ul class="list-group w-50">
                                        <li class="list-group-item"><strong>Ingredients list:</strong></li>

                                        @foreach($ingredients as $ingredient)
                                            <li class="list-group-item">- {{ $ingredient->name }}</li>
                                        @endforeach
                                    </ul>

                                </div>
                            </div>
                        </div>

                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection
