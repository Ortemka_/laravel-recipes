<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Recipe extends Model
{
    use HasFactory;

    protected $fillable = ['name', 'description', 'image', 'dish_type_id', 'author_id'];

    public function dishType()
    {
        return $this->belongsTo(DishType::class);
    }

    public function ingredients()
    {
        return $this->belongsToMany(Ingredient::class, 'recipe_ingredient');
    }

    public function author()
    {
        return $this->belongsTo(User::class, 'author_id');
    }
}
