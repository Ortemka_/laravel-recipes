<?php

namespace App\Policies;

use App\Models\Recipe;
use App\Models\User;

class RecipePolicy
{
    public function user(User $user)
    {
        return $user->role->name === 'user';
    }

    public function author(User $user, Recipe $recipe)
    {
        return $user->id === $recipe->author_id;
    }
}
