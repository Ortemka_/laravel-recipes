<?php

namespace App\Policies;

use App\Models\User;

class IngredientPolicy
{
    public function admin(User $user)
    {
        return $user->role->name === 'admin';
    }
}
