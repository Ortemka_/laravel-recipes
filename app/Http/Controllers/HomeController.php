<?php

namespace App\Http\Controllers;

use App\Models\Recipe;
use Illuminate\Support\Facades\Auth;

class HomeController extends Controller
{
    private $recipe;

    public function __construct(Recipe $recipe)
    {
        $this->recipe = $recipe;
    }

    public function index()
    {
        $recipes = $this->recipe::with(['dishType', 'author'])->orderBy('created_at', 'desc')->paginate(5);

        return view('home')->with(['recipes' => $recipes]);
    }

    public function show(Recipe $recipe)
    {
        if (Auth::id() == $recipe->author_id) {
            return to_route('user.recipe.show', $recipe);
        }

        $ingredients = $recipe->ingredients()->get();

        return view('show')->with(['recipe' => $recipe, 'ingredients' => $ingredients]);
    }
}
