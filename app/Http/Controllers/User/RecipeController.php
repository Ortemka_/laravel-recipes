<?php

namespace App\Http\Controllers\User;

use App\Http\Controllers\Controller;
use App\Models\Recipe;

class RecipeController extends Controller
{
    /**
     * Display a listing of the resource.
     */
    public function index()
    {
        $this->authorize('user', Recipe::class);

        $recipes = auth()->user()->recipes()->with(['dishType'])->orderBy('created_at', 'DESC')->paginate(5);

        return view('user.index', compact('recipes'));
    }

    /**
     * Show the form for creating a new resource.
     */
    public function create()
    {
        $this->authorize('user', Recipe::class);

        $userId = auth()->user()->id;

        return view('user.create', compact('userId'));
    }

    /**
     * Display the specified resource.
     */
    public function show(Recipe $recipe)
    {
        $this->authorize('author', $recipe);

        $ingredients = $recipe->ingredients()->get();

        return view('user.show', compact('recipe', 'ingredients'));
    }

    /**
     * Show the form for editing the specified resource.
     */
    public function edit(Recipe $recipe)
    {

        $this->authorize('author', $recipe);

        $ingredients = $recipe->ingredients();

        $userId = auth()->user()->id;

        return view('user.edit')->with([
            'recipeId' => $recipe->id,
            'userId' => $userId,
            'oldIngredients' => $ingredients,
        ]);
    }

    /**
     * Remove the specified resource from storage.
     */
    public function destroy(Recipe $recipe)
    {
        $this->authorize('author', $recipe);

        $recipe->delete();

        return redirect()->route('user.recipe.index');
    }
}
