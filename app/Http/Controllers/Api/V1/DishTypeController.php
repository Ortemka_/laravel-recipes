<?php

namespace App\Http\Controllers\Api\V1;

use App\Http\Controllers\Controller;
use App\Http\Resources\V1\DishTypeCollection;
use App\Http\Resources\V1\DishTypeResource;
use App\Models\DishType;

class DishTypeController extends Controller
{
    public function index()
    {
        return new DishTypeCollection(DishType::all());
    }

    public function show(DishType $dishType)
    {
        return new DishTypeResource($dishType);
    }
}
