<?php

namespace App\Http\Controllers\Api\V1;

use App\Http\Controllers\Controller;
use App\Http\Requests\RecipeRequest;
use App\Http\Requests\RecipeUpdateRequest;
use App\Http\Resources\V1\RecipeShowResource;
use App\Models\Recipe;

class RecipeController extends Controller
{
    /**
     * Store a newly created resource in storage.
     */
    public function store(RecipeRequest $request)
    {
        $data = $request->validated();

        $path = 'public/images/recipes';
        $imageName = $request->file('image')->getClientOriginalName();
        $request->file('image')->storeAs($path, $imageName);
        $data['image'] = $imageName;

        $recipe = Recipe::create($data);

        $ingredients = explode(',', $data['ingredients']);
        $recipe->ingredients()->attach($ingredients);

        return $recipe;

    }

    /**
     * Display the specified resource.
     */
    public function show(Recipe $recipe)
    {
        $recipe->with('ingredients');

        return new RecipeShowResource($recipe);
    }

    /**
     * Update the specified resource in storage.
     */
    public function update(RecipeUpdateRequest $request, Recipe $recipe)
    {
        $data = $request->validated();
        $ingredients = explode(',', $data['ingredients']);

        unset($data['ingredients']);

        if (isset($data['image'])) {
            $path = 'public/images/recipes';
            $imageName = $request->file('image')->getClientOriginalName();
            $request->file('image')->storeAs($path, $imageName);
            $data['image'] = $imageName;
        }

        $recipe->ingredients()->detach();
        $recipe->update($data);
        $recipe->ingredients()->attach($ingredients);

        return $recipe;
    }
}
