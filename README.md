# Recipes


#### Description: A small website for storing and creating recipes


### Features
- List of all recipes on the main page
- Creating a new recipe (Vue)
- Editing your already created recipes
- Deleting recipes
- Base seeding command - `artisan migrate:fresh --seed`
- PHPUnit tests - `artisan test`
- Login information after seeding the database
- Email Address: `user@user.com`
- Password: `password`
- For correct display, need to transfer an image called `example.jpg` to the folder `storage/app/public/images/recipes`

<br>

Unfortunately, I didn’t have enough time to implement my initial ideas (


